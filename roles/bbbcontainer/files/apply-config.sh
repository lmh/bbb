#!/bin/bash

# Pull in the helper functions for configuring BigBlueButton
source /etc/bigbluebutton/bbb-conf/apply-lib.sh

echo "  - Setting camera defaults"
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[0].bitrate 50
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[1].bitrate 100
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[2].bitrate 200
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[3].bitrate 300

yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[0].default true
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[1].default false
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[2].default false
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[3].default false
chown meteor:meteor $HTML5_CONFIG

enableMultipleKurentos

## configure some stuff:
echo "  - Disable recordings"
sed -i "s/^disableRecordingDefault=.*$/disableRecordingDefault=true/" /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
echo "  - Disable freeswitch IO scheduling optimization"
sed -ri -e "s/(^IOScheduling.*)/#\1/" /lib/systemd/system/freeswitch.service
systemctl daemon-reload

if [ -e /var/www/bigbluebutton-default/index.html ] ; then 
        echo "  - Remove useless BBB index.html page"
        mv -f /var/www/bigbluebutton-default/index.html /var/www/bigbluebutton-default/index.html.orig
fi
