#!/bin/bash
 
sas=$(grep ^static-auth-secret /etc/turnserver.conf | cut -d= -f2)
tme=$(date +%s)
exp=86400
username=$(( $tme + $exp ))
pass=$(echo -n $username | openssl dgst -binary -sha1 -hmac $sas | openssl base64)
 
 
echo "User: $username"
echo "Pass: $pass"
